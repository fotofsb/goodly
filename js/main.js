$(document).ready(function () {
  $("#modal-show").click(function () {
    $("#modal").fadeIn();
  });
  $("#close").click(function () {
    $("#modal").fadeOut();
  });
});